namespace Model
{
    public class TimeEntry
    {
        public string User { get; set; }
        public string Project { get; set; }

        public string Task { get; set; }
        public string Observation { get; set; }

        public string Time { get; set; }

        public string Date { get; set; }

        public TimeEntry()
        {

        }
    }
}
