namespace Model
{
    public class TimecampItem
    {
        public string Date { get; set; }
        public string Activity { get; set; }

        public string Comment { get; set; }

        public string Project { get; set; }

        public string Task { get; set; }
        public string Time { get; set; }

        public string Ticket { get; set; }

    }
}
